# -*- coding: utf-8 -*-

# Copyright (C) 2014 Matthias Mengel and Carl-Friedrich Schleussner
#
# This file is part of wacalc.
#
# wacalc is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# wacalc is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License
# along with wacalc; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


""" database setting file. """

import sys,glob,os,pickle,string
import numpy as np
from netCDF4 import Dataset,num2date
import pandas as pd
import pycountry


# basepath is the directory where the app is located
basepath='/Users/peterpfleiderer/Box/Website - Online tools/localslr/'

try:
  os.chdir(basepath)
except:
  basepath='/home/tooli/localslr/'
  os.chdir(basepath)


inland_grids=['grid_14.0_16.0','grid_12.0_16.0','grid_14.0_14.0','grid_12.0_14.0','grid_0.0_32.0','grid_-2.0_32.0','grid_-0.0_34.0','grid_-2.0_34.0','grid_-4.0_30.0','grid_-6.0_30.0','grid_-8.0_30.0','grid_-8.0_32.0','grid_-10.0_34.0','grid_-12.0_34.0','grid_-14.0_34.0','grid_-12.0_36.0','grid_-14.0_36.0','grid_46.0_58.0','grid_46.0_60.0','grid_46.0_62.0','grid_44.0_58.0','grid_44.0_60.0','grid_44.0_62.0','grid_56.0_110.0','grid_54.0_110.0','grid_54.0_108.0','grid_52.0_108.0','grid_52.0_106.0','grid_52.0_106.0','grid_52.0_104.0','grid_62.0_242.0','grid_62.0_244.0','grid_62.0_246.0','grid_62.0_248.0','grid_62.0_250.0','grid_62.0_252.0','grid_60.0_244.0','grid_60.0_246.0','grid_60.0_250.0','grid_60.0_252.0','grid_64.0_238.0','grid_58.0_248.0','grid_58.0_250.0','grid_56.0_256.0','grid_56.0_258.0','grid_58.0_258.0','grid_54.0_260.0','grid_54.0_262.0','grid_52.0_262.0','grid_50.0_262.0','grid_52.0_264.0','grid_50.0_264.0','grid_48.0_268.0','grid_48.0_270.0','grid_48.0_272.0','grid_48.0_274.0','grid_48.0_276.0','grid_50.0_272.0','grid_46.0_268.0','grid_46.0_270.0','grid_46.0_272.0','grid_46.0_274.0','grid_46.0_276.0','grid_46.0_278.0','grid_46.0_280.0','grid_44.0_272.0','grid_44.0_274.0','grid_44.0_276.0','grid_44.0_278.0','grid_44.0_280.0','grid_44.0_282.0','grid_44.0_284.0','grid_42.0_272.0','grid_42.0_274.0','grid_42.0_276.0','grid_42.0_278.0','grid_42.0_280.0','grid_42.0_282.0']

# read original
dat1=pd.read_table('data/LSLproj_griddedRCPs.tsv', sep='\t')
dat1.columns = [u'Site Name',u'Site ID',u'Scenario',u'Year',0.005,0.01,0.05,0.167,0.5,0.833,0.95,0.99,0.995,0.999]
dat1 = dat1.loc[:,[u'Site Name',u'Site ID',u'Scenario',u'Year',0.01,0.05,0.167,0.5,0.833,0.95,0.99,0.995,0.999]]
dat2=pd.read_csv('data/bamber19.tsv', sep='\t')
dat2.reset_index(inplace=True)
dat2.columns = dat2.iloc[0,:]
dat2 = dat2.iloc[1:,:]
dat2.columns = [u'Site Name',u'Site ID',u'Scenario',u'Year',0.01,0.05,0.167,0.5,0.833,0.95,0.99,0.995,0.999]
dat3=pd.read_csv('data/rasmussen18.tsv', sep='\t')
dat3.reset_index(inplace=True)
dat3.columns = dat3.iloc[0,:]
dat3 = dat3.iloc[1:,:]
dat3.columns = [u'Site Name',u'Site ID',u'Scenario',u'Year',0.01,0.05,0.167,0.5,0.833,0.95,0.99,0.995,0.999]

slr = pd.concat((dat1,dat2,dat3), ignore_index=True, sort='Site ID')
slr['Year'] = np.array(slr['Year'], np.int)
slr['Site ID'] = np.array(slr['Site ID'], np.int)

to_be_removed=pd.read_csv('data/STATION_IDs_PSMSL_to_be_removed.csv', skiprows=1, sep=',')
used_ids = np.unique([i for i in slr['Site ID'] if ((i<10000) | (i>1000000000)) & (i not in list(to_be_removed.STATION_ID.values))])

print(len(used_ids))
print(list(to_be_removed.STATION_ID.values))

# stations
stat = pd.read_table('data/Station_LatLon.txt')
names = [' '.join([nnn__.capitalize() for nnn__ in nn.split(' ')]).replace('Ii','II').replace('Iii','III').replace('IIi','III') for nn in stat['name'][:]]
stat['name'] = names

station_lons,station_lats,station_names=[],[],[]
grid_xmin,grid_xmax,grid_ymin,grid_ymax,grid_names=[],[],[],[],[]
for i,name in enumerate(names):
	if stat['ID'][i] in used_ids:
		if 'Grid' not in name.split('_'):
			station_names.append(name)
			station_lons.append(float(stat['lon'][i]))
			station_lats.append(float(stat['lat'][i]))

		else:
			grid_names.append(name)
			grid_xmin.append(float(stat['lon'][i]-1))
			grid_xmax.append(float(stat['lon'][i]+1))
			grid_ymin.append(float(stat['lat'][i]-1))
			grid_ymax.append(float(stat['lat'][i]+1))

print(station_names)


scenario_dict = {
	#'1p5degree':	{'color':'#23B5E8', 'alpha':0.2, 'lsty':'-', 'name':'1.5°C'},
	'2p0degree+L':	{'color':'#344BED', 'alpha':0.0, 'lsty':'--', 'name':'< 2°C (+expert judgement)'},
	'rcp26':		{'color':'#344BED', 'alpha':0.2, 'lsty':'-', 'name':'< 2°C'},
	'rcp45':		{'color':'#F97306', 'alpha':0.2, 'lsty':'-', 'name':'+2.5°C'},
	#'rcp60':		{'color':'orange', 'alpha':0.2, 'lsty':'-', 'name':'bla'},
	'rcp85':		{'color':'#EA514B', 'alpha':0.2, 'lsty':'-', 'name':'> 4°C'},
	'rcp85+H':		{'color':'#EA514B', 'alpha':0.0, 'lsty':'--', 'name':'> 4°C (+expert judgement)'},
}


#
