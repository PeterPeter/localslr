import os,sys,glob
import pandas as pd
import numpy as np
import xarray as xr

import matplotlib.pylab as plt
from matplotlib import rcParams
import seaborn as sns; sns.set()

# read original
dat1=pd.read_table('data/LSLproj_griddedRCPs.tsv')
dat1.columns = [u'Site Name',u'Site ID',u'Scenario',u'Year',0.005,0.01,0.05,0.167,0.5,0.833,0.95,0.99,0.995,0.999]
dat1 = dat1.loc[:,[u'Site Name',u'Site ID',u'Scenario',u'Year',0.01,0.05,0.167,0.5,0.833,0.95,0.99,0.995,0.999]]
dat2=pd.read_table('data/bamber19.tsv')
dat2.reset_index(inplace=True)
dat2.columns = dat2.iloc[0,:]
dat2 = dat2.iloc[1:,:]
dat2.columns = [u'Site Name',u'Site ID',u'Scenario',u'Year',0.01,0.05,0.167,0.5,0.833,0.95,0.99,0.995,0.999]
dat3=pd.read_table('data/rasmussen18.tsv')
dat3.reset_index(inplace=True)
dat3.columns = dat3.iloc[0,:]
dat3 = dat3.iloc[1:,:]
dat3.columns = [u'Site Name',u'Site ID',u'Scenario',u'Year',0.01,0.05,0.167,0.5,0.833,0.95,0.99,0.995,0.999]

dat = pd.concat((dat1,dat2,dat3), ignore_index=True, sort='Site ID')
dat['Year'] = np.array(dat['Year'], np.int)
dat['Site ID'] = np.array(dat['Site ID'], np.int)

to_be_removed=pd.read_table('data/STATION_IDs_PSMSL_to_be_removed.csv', skiprows=1, sep=',')

used_ids = np.unique([i for i in dat['Site ID'] if ((i<10000) | (i>1000000000)) & (i not in to_be_removed.STATION_ID.values)])

scenario_dict = {
	#'1p5degree':	{'color':'#23B5E8', 'alpha':0.2, 'lsty':'-', 'name':'1.5°C'},
	'2p0degree+L':	{'color':'#344BED', 'alpha':0.0, 'lsty':'--', 'name':'< +2°C (+ expert judgment on extra Antarctic contribution)'},
	'rcp26':		{'color':'#344BED', 'alpha':0.2, 'lsty':'-', 'name':'< +2°C'},
	'rcp45':		{'color':'orange', 'alpha':0.2, 'lsty':'-', 'name':'~ +2.5°C'},
	#'rcp60':		{'color':'orange', 'alpha':0.2, 'lsty':'-', 'name':'bla'},
	'rcp85':		{'color':'#EA514B', 'alpha':0.2, 'lsty':'-', 'name':'> +4°C'},
	'rcp85+H':		{'color':'#EA514B', 'alpha':0.0, 'lsty':'--', 'name':'> +4°C (+ expert judgment on extra Antarctic contribution)'},
}

plt.close()
plt.plot(np.unique(dat['Site ID']))
plt.savefig('station_ID.pdf')

for i in used_ids:
	if os.path.isfile('app/static/plots/'+str(i)+'.png')==False or True:
		plt.close()
		fig = plt.figure(figsize=(5,4))
		plt.plot([2000,2200],[0,0],'k--')
		handles,labels = [],[]
		for scenario in ['rcp26','2p0degree+L','rcp45','rcp85','rcp85+H']:
			details = scenario_dict[scenario]
			tmp = dat.loc[((dat['Site ID']==i) & (dat['Scenario']==scenario)),:]
			patch_ = plt.fill_between(tmp.Year[0:20],tmp[0.167][0:20],tmp[0.833][0:20],alpha=details['alpha'],color=details['color'])
			line_ = plt.plot(tmp.Year[0:20],tmp[0.5][0:20],color=details['color'], linestyle=details['lsty'])[0]
			labels.append(details['name'])
			handles.append((patch_,line_))
		plt.ylabel('Sea level rise [cm]')
		plt.legend(handles, labels, loc='upper left', fontsize=8)
		plt.tight_layout()
		title=tmp['Site Name'].values[0].replace('grid_',' lat ').replace('_',' lon ')
		title=' '.join([nnn__.capitalize() for nnn__ in title.split(' ')])
		plt.title(title)
		plt.tight_layout()
		plt.savefig('app/static/plots/'+str(i)+'.png',dpi=200)
		plt.savefig('app/static/plots/'+str(i)+'.pdf')






#
