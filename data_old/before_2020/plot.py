import sys,glob,os,pickle,string
import numpy as np
import dimarray as da
import itertools
import seaborn as sns
import pandas as pd

from shapely.geometry import mapping, Polygon, MultiPolygon
import matplotlib.pylab as plt
from matplotlib import rcParams
import seaborn as sns; sns.set()

os.chdir('/Users/peterpfleiderer/Documents/Projects/sea_level_web/')

overwrite=True


# stations
stat=np.loadtxt('data/Station_LatLon.txt', delimiter='\t', dtype={'names': ('ID','name','lon','lat'), 'formats': (np.int, '|S15', np.float, np.float)}, skiprows=1)
station_dict={}
ID_dict={}
for i in range(stat.shape[0]):
    station_dict[stat[i][0]]=stat[i]
    ID_dict[stat[i][1]]=stat[i][0]


slr=da.read_nc('data/slr.nc')['slr']
# plot all
for i in slr.ID:
    if os.path.isfile('app/static/plots/'+str(i)+'.png')==False or overwrite:
        plt.close()
        fig = plt.figure(figsize=(5,4))
        plt.plot([2000,2200],[0,0],'k--')
        for rcp,color,label in zip(['rcp85','rcp45','rcp26'],['red','orange','green'],['> +4$^\circ$C','+2.5$^\circ$C','Paris compatible']):
            plt.fill_between(slr.decade[0:20],slr[i,rcp,0.167].ix[0:20],slr[i,rcp,0.833].ix[0:20],alpha=0.2,color=color)
            plt.plot(slr.decade[0:20],slr[i,rcp,0.5].ix[0:20],color=color,label=label)
        plt.ylabel('Sea level rise [cm]')
        plt.legend(loc='upper left')
        plt.tight_layout()
        title=station_dict[i][1].replace('grid_',' lat ').replace('_',' lon ')
        print(title)
        title=' '.join([nnn__.capitalize() for nnn__ in title.split(' ')])
        print(i,title,station_dict[i][1])
        plt.title(title)
        plt.tight_layout()
        plt.savefig('app/static/plots/'+str(i)+'.png',dpi=100)
        plt.savefig('app/static/plots/'+str(i)+'.pdf')


np.sum([i in slr.ID for i in range(3000000000,3000000100)])
# additional plots:
copied={
    'grid_18.0_298.0':('grid_16.0_298.0'),
    'grid_18.0_296.0':('grid_18.0_294.0'),
    'grid_12.0_298.0':('grid_14.0_298.0'),
    'grid_12.0_300.0':('grid_14.0_300.0'),
    'grid_12.0_294.0':('grid_10.0_294.0'),
    'grid_12.0_296.0':('grid_10.0_296.0'),
    'grid_22.0_288.0':('grid_22.0_286.0')
}


slr_new=da.DimArray(axes=[np.array(range(len(copied.keys())))+3000000000,slr.rcp,slr.quantile,slr.decade],dims=['ID','rcp','quantile','decade'])

for i_new,grid_cell_new,grid_cell_copied in zip(np.array(range(len(copied.keys())))+3000000000,copied.keys(),copied.values()):
    i=ID_dict[grid_cell_copied]
    if os.path.isfile('app/static/plots/'+str(i)+'.png')==False or overwrite:
        plt.close()
        fig = plt.figure(figsize=(5,4))
        plt.plot([2000,2200],[0,0],'k--')
        for rcp,color,label in zip(['rcp85','rcp45','rcp26'],['red','orange','green'],['> +4$^\circ$C','+2.5$^\circ$C','Paris compatible']):
            plt.fill_between(slr.decade[0:20],slr[i,rcp,0.167].ix[0:20],slr[i,rcp,0.833].ix[0:20],alpha=0.2,color=color)
            plt.plot(slr.decade[0:20],slr[i,rcp,0.5].ix[0:20],color=color,label=label)
        plt.ylabel('Sea level rise [cm]')
        plt.legend(loc='upper left')
        plt.tight_layout()
        title=grid_cell_new.replace('grid_',' lat ').replace('_',' lon ')
        title=' '.join([nnn__.capitalize() for nnn__ in title.split(' ')])
        plt.title(title)
        plt.tight_layout()
        plt.savefig('app/static/plots/'+str(i_new)+'.png',dpi=100)
        plt.savefig('app/static/plots/'+str(i_new)+'.pdf')


dat=pd.read_table('data/LSLproj_griddedRCPs.tsv')

# transform to dimarray
stations=sorted(set(dat['STATION']))
IDs=list(np.array(range(len(copied.keys())))+3000000000)
rcps=sorted(set(dat['RCP']))
decades=sorted(set(dat['DECADE']))
quantiles=[0.005,0.01,0.05,0.167,0.5,0.833,0.95,0.99,0.995,0.999]

slr=da.DimArray(axes=[IDs,rcps,quantiles,decades],dims=['ID','rcp','quantile','decade'])

for i_new,grid_cell_new,grid_cell_copied in zip(np.array(range(len(copied.keys())))+3000000000,copied.keys(),copied.values()):
    i=ID_dict[grid_cell_copied]
    for rcp in slr.rcp:
        for qu in slr.quantile:
            tmp=dat[(dat['ID']==i) & (dat['RCP']==rcp)]
            slr[i_new,rcp,qu,tmp['DECADE']]=tmp[str(qu)]


ds=da.Dataset({'slr':slr})
ds.write_nc('data/slr_copied.nc', mode='w')


# stations
stations=da.DimArray(axes=[[gr_name.capitalize() for gr_name in copied.keys()],['ID','lon','lat','tide']],dims=['name','info'])
for i_new,name in zip(IDs,stations.name):
    lon__=float(name.split('_')[-1])
    if lon__>180:lon__-=360
    stations[name]=[i_new,lon__,name.split('_')[-2],False]

ds=da.Dataset({'stations':stations})
ds.write_nc('data/stations_copied.nc', mode='w')
