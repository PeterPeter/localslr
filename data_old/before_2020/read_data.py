import os,sys,glob
import pandas as pd
import dimarray as da



# read original
dat=pd.read_table('data/LSLproj_griddedRCPs.tsv')

# transform to dimarray
stations=sorted(set(dat['STATION']))
IDs=sorted(set(dat['ID']))
rcps=sorted(set(dat['RCP']))
decades=sorted(set(dat['DECADE']))
quantiles=[0.005,0.01,0.05,0.167,0.5,0.833,0.95,0.99,0.995,0.999]


slr=da.DimArray(axes=[IDs,rcps,quantiles,decades],dims=['ID','rcp','quantile','decade'])
for i in sorted(set(dat['ID'])):
    for rcp in slr.rcp:
        for qu in slr.quantile:
            tmp=dat[(dat['ID']==i) & (dat['RCP']==rcp)]
            slr[i,rcp,qu,tmp['DECADE']]=tmp[str(qu)]


ds=da.Dataset({'slr':slr})
ds.write_nc('data/slr.nc', mode='w')



# stations
stat=pd.read_table('data/Station_LatLon.txt')
stations=da.DimArray(axes=[[' '.join([nnn__.capitalize() for nnn__ in nn.split(' ')]) for nn in stat['name'][:]],['ID','lon','lat','tide']],dims=['name','info'])
for i,name in zip(range(len(stat['ID'])),stations.name):
    stations[name]=[stat['ID'][i],stat['lon'][i],stat['lat'][i],'grid' not in stat['name'][i].split('_')]

ds=da.Dataset({'stations':stations})
ds.write_nc('data/stations.nc', mode='w')



'''
old
'''

# # # stations
# # stat=pd.read_table('data/Station_LatLon.txt')
# # stations=da.DimArray(axes=[list(stat['name'][:]),['ID','lon','lat','tide']],dims=['name','info'])
# # for i in range(len(stat['ID'])):
# #     stations[stat['name'][i]]=[stat['ID'][i],stat['lon'][i],stat['lat'][i],'grid' not in stat['name'][i].split('_')]
# #
# # ds=da.Dataset({'stations':stations})
# # ds.write_nc('data/stations.nc', mode='w')
# #
#
# # stations
# stat=np.loadtxt('data/Station_LatLon.txt', delimiter='\t', dtype={'names': ('ID','name','lon','lat'), 'formats': (np.int, '|S15', np.float, np.float)}, skiprows=1)
# station_dict={}
# ID_dict={}
# for i in range(stat.shape[0]):
#     station_dict[stat[i][0]]=stat[i]
#     ID_dict[stat[i][1]]=stat[i][0]
#
# output = open('data/station_dict.pkl', 'wb')
# pickle.dump(station_dict, output)
# output.close()
