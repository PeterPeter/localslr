import os,sys,glob
import pandas as pd
import dimarray as da
import numpy as np
import matplotlib.pylab as plt
import seaborn as sns
sns.set()

os.chdir('/Users/peterpfleiderer/Documents/Projects/sea_level_web/')
slr=da.read_nc('data/slr.nc')['slr']


# stations
stat=np.loadtxt('data/Station_LatLon.txt', delimiter='\t', dtype={'names': ('ID','name','lon','lat'), 'formats': (np.int, '|S15', np.float, np.float)}, skiprows=1)
station_dict={}
ID_dict={}
for i in range(stat.shape[0]):
    station_dict[stat[i][0]]=stat[i]
    ID_dict[stat[i][1]]=stat[i][0]

# transform to dimarray
contributions=['AIS','GIS','Ocean','GIC','LWS','Bkgd']
ids=list(set([int(filename.split('_')[-2]) for filename in glob.glob('data/LSLproj_decompGRIDDED/*')]))

decomp=da.DimArray(axes=[ids,slr.rcp,contributions,slr.quantile,slr.decade],dims=['ID','rcp','contribution','quantile','decade'])


print('detecting\n10------50-------100')
for id_,progress in zip(ids,np.array([['-']+['']*(len(ids)/20+1)]*20).flatten()[0:len(ids)]):
    sys.stdout.write(progress); sys.stdout.flush()
    for rcp in slr.rcp:
        filename='data/LSLproj_decompGRIDDED/LSLproj_decomp_'+str(id_)+'_'+rcp+'.tsv'
        if os.path.isfile(filename):
            lines=open(filename,'r').read().split('\n')
            for i,line in enumerate(lines):
                if line in [str(year) for year in slr.decade]:
                    for tmp in lines[i+1:i+7]:
                        tmp=tmp.split('\t')
                        for qu_i,qu_name in enumerate(decomp.quantile):
                            decomp[id_,rcp,tmp[0],qu_name,int(line)]=tmp[qu_i+1]




            tmp=decomp[id_,rcp]

            sourVar=np.zeros([len(tmp.contribution)+2,len(tmp.decade)])
            for contr,color,i in zip(tmp.contribution,['r','c','b','g','m','y'],range(len(tmp.contribution))):
                for dec,t in zip(tmp.decade,range(len(tmp.decade))):
                    p=[min([qu,1-qu]) for qu in tmp.quantile]
                    ex=np.sum([xx*pp for xx,pp in zip(tmp[contr,:,dec],p)])
                    ex2=np.sum([xx*xx*pp for xx,pp in zip(tmp[contr,:,dec],p)])
                    # where is this factor of 0.1 coming from?
                    sourVar[i+2,t]=(ex2-ex**2)*0.0001


            plt.close('all')
            fig,axes = plt.subplots(figsize=(7,3),ncols=2)
            for contr,color,i in zip(tmp.contribution,['r','c','b','g','m','y'],range(len(tmp.contribution))):
                axes[0].fill_between(tmp.decade[0:10],np.sum(sourVar[0:i+2,0:10],axis=0),np.sum(sourVar[0:i+3,0:10],axis=0),color=color,alpha=1,label=contr)
                axes[1].fill_between(tmp.decade[0:10],np.sum(sourVar[0:i+2,0:10],axis=0)/np.sum(sourVar[:,0:10],axis=0),np.sum(sourVar[0:i+3,0:10],axis=0)/np.sum(sourVar[:,0:10],axis=0),color=color,alpha=1,label=contr)

            axes[0].set_ylabel('variance in sea level rise [m$^2$]')
            axes[1].set_ylabel('fraction of variance')
            axes[0].legend(loc='upper left')
            axes[0].set_xlim((2010,2100))
            axes[0].set_ylim(ymin=0)
            axes[1].set_xlim((2010,2100))
            axes[1].set_ylim((0,1))
            plt.suptitle(station_dict[id_][1].replace('grid_',' lat ').replace('_',' lon ')+' '+rcp.replace('rcp','RCP'))
            plt.tight_layout(rect=(0,0,1,0.95))
            plt.savefig('app/static/decomposition_plots/'+str(id_)+'_'+rcp+'.png')


ds=da.Dataset({'decomp':decomp})
ds.write_nc('data/decomp.nc', mode='w')

copied={
    'grid_18.0_298.0':('grid_16.0_298.0'),
    'grid_18.0_296.0':('grid_18.0_294.0'),
    'grid_12.0_298.0':('grid_14.0_298.0'),
    'grid_12.0_300.0':('grid_14.0_300.0'),
    'grid_12.0_294.0':('grid_10.0_294.0'),
    'grid_12.0_296.0':('grid_10.0_296.0'),
    'grid_22.0_288.0':('grid_22.0_286.0')
}

for i_new,grid_cell_new,grid_cell_copied in zip(np.array(range(len(copied.keys())))+3000000000,copied.keys(),copied.values()):
    id_=ID_dict[grid_cell_copied]
    for rcp in slr.rcp:
        filename='data/LSLproj_decompGRIDDED/LSLproj_decomp_'+str(id_)+'_'+rcp+'.tsv'
        if os.path.isfile(filename):
            lines=open(filename,'r').read().split('\n')
            for i,line in enumerate(lines):
                if line in [str(year) for year in slr.decade]:
                    for tmp in lines[i+1:i+7]:
                        tmp=tmp.split('\t')
                        for qu_i,qu_name in enumerate(decomp.quantile):
                            decomp[id_,rcp,tmp[0],qu_name,int(line)]=tmp[qu_i+1]




            tmp=decomp[id_,rcp]

            sourVar=np.zeros([len(tmp.contribution)+2,len(tmp.decade)])
            for contr,color,i in zip(tmp.contribution,['r','c','b','g','m','y'],range(len(tmp.contribution))):
                for dec,t in zip(tmp.decade,range(len(tmp.decade))):
                    p=[min([qu,1-qu]) for qu in tmp.quantile]
                    ex=np.sum([xx*pp for xx,pp in zip(tmp[contr,:,dec],p)])
                    ex2=np.sum([xx*xx*pp for xx,pp in zip(tmp[contr,:,dec],p)])
                    # where is this factor of 0.1 coming from?
                    sourVar[i+2,t]=(ex2-ex**2)*0.0001


            plt.close('all')
            fig,axes = plt.subplots(figsize=(7,3),ncols=2)
            for contr,color,i in zip(tmp.contribution,['r','c','b','g','m','y'],range(len(tmp.contribution))):
                axes[0].fill_between(tmp.decade[0:10],np.sum(sourVar[0:i+2,0:10],axis=0),np.sum(sourVar[0:i+3,0:10],axis=0),color=color,alpha=1,label=contr)
                axes[1].fill_between(tmp.decade[0:10],np.sum(sourVar[0:i+2,0:10],axis=0)/np.sum(sourVar[:,0:10],axis=0),np.sum(sourVar[0:i+3,0:10],axis=0)/np.sum(sourVar[:,0:10],axis=0),color=color,alpha=1,label=contr)

            axes[0].set_ylabel('variance in sea level rise [m$^2$]')
            axes[1].set_ylabel('fraction of variance')
            axes[0].legend(loc='upper left')
            axes[0].set_xlim((2010,2100))
            axes[0].set_ylim(ymin=0)
            axes[1].set_xlim((2010,2100))
            axes[1].set_ylim((0,1))
            plt.suptitle(grid_cell_new.replace('grid_',' lat ').replace('_',' lon ')+' '+rcp.replace('rcp','RCP'))
            plt.tight_layout(rect=(0,0,1,0.95))
            plt.savefig('app/static/decomposition_plots/'+str(i_new)+'_'+rcp+'.png')
