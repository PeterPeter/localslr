import sys,glob,os,pickle,string
import numpy as np
import xarray as xr
import itertools
import seaborn as sns
import pandas as pd

from shapely.geometry import mapping, Polygon, MultiPolygon
import matplotlib.pylab as plt
from matplotlib import rcParams
import seaborn as sns; sns.set()

os.chdir('/Users/peterpfleiderer/Box/Website - Online tools/localslr/data/')


# stations
stat=np.loadtxt('Station_LatLon.txt', delimiter='\t', dtype={'names': ('ID','name','lon','lat'), 'formats': (np.int, '|S15', np.float, np.float)}, skiprows=1)

lats,lons = [],[]
for i in range(stat.shape[0]):
    if stat[i][1][:3] == b'gri':
        lats.append(stat[i][2])
        lons.append(stat[i][3])

slr=xr.open_dataset('slr.nc')['slr']
coords = dict(rcp=slr.rcp, quantile=slr['quantile'], decade=slr.decade, lat=np.arange(-90,92,2), lon=np.arange(-178,182,2))
slr_gridded = xr.DataArray(np.zeros([len(slr.rcp),len(slr['quantile']),len(slr.decade),len(coords['lat']),len(coords['lon'])])*np.nan,
                           coords=coords, dims=['rcp','quantile','decade','lat','lon'])

for i in range(stat.shape[0]):
    if stat[i][1][:3] == b'gri':
        y,x = stat[i][2], stat[i][3]
        if x>180:
            x-=360
        if stat[i][0] in slr.ID:
            slr_gridded.loc[:,:,:,y,x] = slr.loc[stat[i][0]]
        else:
            print(stat[i][0])

xr.Dataset({'slr_gridded':slr_gridded}).to_netcdf('/Users/peterpfleiderer/Box/Website - Online tools/localslr/data/slr_gridded.nc')
